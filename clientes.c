#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include "lista.h"
#include "ficheiros.h"
#include "interface.h"

/*cores*/
#define azul_escuro 1
#define verde_escuro 2
#define esverdeado 3
#define vermelho_escuro 4
#define dourado 6
#define original 7
#define cinzento 8
#define azul 9
#define verde 10
#define azul_claro 11
#define vermelho 12
#define amarelo 14
#define branco 15

/*menu especifico para adicionar novo cliente ou escolher cliente existente*/
int menu_cliente(char *texto){
	int opcao;
	system("cls");
	ecran();
	cortexto(amarelo);
	gotoxy(25,3);printf("Criar %s",texto);
	cortexto(original);
	gotoxy(13,8);printf("1 - > Adicionar novo cliente");
	gotoxy(13,9);printf("2 - > Seleccionar cliente ja existente");
    gotoxy(13,11);printf("0 - > Voltar");
    cortexto(verde);
    gotoxy(7,22);printf("Escolha uma op%c%co: ",135,132);
	cortexto(original);
	fflush(stdin);

    opcao=verifica_opcao('0','2',26,22);

    return opcao;
}

List_Client pesquisa_cliente (List_Client c, char *nome)
{
    List_Client actual,ant;

    for(ant=c,actual=c->next; actual!=NULL; actual=actual->next,ant=ant->next)
        if(strcmp(actual->cliente->nome,nome)>0)
            break;

    return ant;
}

Client pesquisa_cliente_nif(List_Client c,long int nif)
{
    List_Client aux;

    if (c->next == NULL)
        return NULL;

    for(aux=c->next;aux!=NULL;aux=aux->next)
        if (aux->cliente->nif == nif)
            return aux->cliente;

    return NULL;
}

Client insere_cliente(List_Client c)
{
    List_Client aux_c,no;
    char nome[50],localidade[50];
    long int nif,telefone;

    system("cls");
    ecran();
    cortexto(amarelo);
    gotoxy(30,3);printf("Adicionar Clientes");
    cortexto(original);
    gotoxy(8,8);printf("Nome: ");
    gotoxy(8,10);printf("Localidade: ");
    gotoxy(8,12);printf("Telefone: ");
    gotoxy(8,14);printf("NIF: ");

    gotoxy(14,8);
    while (strlen(fgets(nome,50,stdin))<3)
    {
        cortexto(vermelho);
        gotoxy(6,22);printf("O nome tem de ter pelo menos 2 caracteres");
        gotoxy(14,8); printf("                                                    ");
        gotoxy(14,8);
        cortexto(original);
    }

    if (nome[strlen(nome)-1]=='\n')
        nome[strlen(nome)-1]='\0';

    gotoxy(6,22);
    printf("                                                     ");

    gotoxy(20,10);
    while (strlen(fgets(localidade,50,stdin))<3)
    {
        cortexto(vermelho);
        gotoxy(6,22);printf("A localidade tem de ter pelo menos 2 caracteres");
        gotoxy(20,10); printf("                                                    ");
        gotoxy(20,10);
        cortexto(original);
    }

    if (localidade[strlen(localidade)-1]=='\n')
        localidade[strlen(localidade)-1]='\0';

    gotoxy(6,22);
    printf("                                               ");

    gotoxy(18,12);
    telefone=verifica_numerosC(c,18,12,"telefone");

    gotoxy(6,22);
    printf("                                               ");
    nif=verifica_numerosC(c,13,14,"NIF");
    no=cria_no_client(nome,localidade,telefone,nif);

    if (c->next==NULL)
        c->next=no;

    else{
        aux_c=pesquisa_cliente(c,nome);
        no->next=aux_c->next;
        aux_c->next=no;
    }

    c->total++;
    actualiza_clientes(no);

    cortexto(verde);
    gotoxy(6,21);printf("Cliente adicionado com sucesso.");
    cortexto(verde_escuro);
    gotoxy(6,22);printf("Prima qualquer tecla para regressar 'a marca%c%co da reserva...",135,132);
    cortexto(original);
    getch();

    return no->cliente;
}

Client lista_cliente(List_Client c, char* titulo)
{
    int i=0,k,total,opcao;
    char opt[6];
    List_Client aux=NULL;

    /*total guarda o numero de elementos da lista*/

    /*quando a lista esta vazia*/
    if (c->total==0)
    {
        system("cls");    /*desenha a moldura*/
        ecran();
        /*desenha o titulo*/
        cortexto(amarelo);
        gotoxy(27,3);printf("%s",titulo);
        /*imprime informacoes*/
        cortexto(vermelho);
        gotoxy(8,8);printf("N%co existem clientes na base de dados!",132);
        cortexto(verde_escuro);
        gotoxy(6,22);printf("Prima qualquer tecla para regressar ao menu...");
        cortexto(original);
        getch();
        return NULL;
    }
    /*quando a lista nao esta vazia*/
    else
    {
        total=c->total;
        aux=c->next;
        do {
            system("cls");
            ecran();
            grelhaListaC();
            /*imprime titulo*/
            cortexto(amarelo);
            gotoxy(27,3);printf("%s",titulo);
            cortexto(original);

             /*imprime a posicao das paginas, para o caso de 1 unica pagina*/
            k=0;
            cortexto(cinzento);
            gotoxy(60,5);printf(" Pagina %d de %d ",1+i/6,1+(total-1)/6);
            cortexto(original);

            if(total>i)
                /*imprime a lista ate um maximo de 6 de cada vez*/
                for (;aux!=NULL;aux=aux->next)
                {
                    i++;
                    k++;
                    gotoxy(3,6+2*k);printf("%d",i);
                    gotoxy(8,6+2*k);printf("%.44s",aux->cliente->nome);
                    gotoxy(54,6+2*k);printf("%ld",aux->cliente->telefone);
                    gotoxy(66,6+2*k);printf("%ld",aux->cliente->nif);

                    if (i%6==0)
                    {
                        aux=aux->next;
                        break;
                    }
                }
            /*le a opcao*/
            cortexto(verde_escuro);
            gotoxy(3,23);printf("Teclas:  (0) -> Sair      (-1)  -> Avancar para a proxima pagina.");
            cortexto(verde);
            gotoxy(3,22);printf("Escolha o n%c do cliente: ",248);
            cortexto(original);

            /*se opcao for invalida*/
            sprintf(opt,"%d",i);
            opcao = verifica_opcao_menu(opt,total,28);

        }while (opcao==-1);
        cortexto(original);

        if (opcao==0)
            return NULL;

        i=1;
        for(aux=c->next;aux!=NULL;aux=aux->next,i++)
            if(i==opcao)
                break;
    }

    return aux->cliente;
}


long int verifica_numerosC(List_Client c,int x,int y,char *texto)
{
    List_Client aux;
    unsigned int n;
    int check=0;
    char numero[12];
    while (check!=1)
    {
        fflush(stdin);
        gotoxy(x,y);
        fgets(numero,12,stdin);
        if ((strlen(numero)-1)!=9){
            check=0;
            gotoxy(6,22);
            cortexto(vermelho);
            printf("O %s tem que conter 9 digitos          ",texto);
            cortexto(original);
            gotoxy(x,y); printf("                    ");
        }
        else
        {
            for (n=0;n<(strlen(numero)-1);n++)
                if (numero[n]<'0' || numero[n]>'9')
                {
                    check=0;
                    gotoxy(6,22);
                    cortexto(vermelho);
                    printf("O %s so pode conter numeros        ",texto);
                    cortexto(original);
                    gotoxy(x,y); printf("                    ");
                    break;
                }
                else
                    check=1;
        }
        if (check==1 && strcmp(texto,"NIF")==0)
            for (aux=c->next;aux!=NULL;aux=aux->next)
                if (aux->cliente->nif==atoi(numero))
                    {
                        check=0;
                        gotoxy(6,22);
                        cortexto(vermelho);
                        printf("Ja existe esse NIF                ");
                        cortexto(original);
                        gotoxy(x,y); printf("                    ");
                        break;
                    }

    }
    return atoi(numero);
}



