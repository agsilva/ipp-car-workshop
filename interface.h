#ifndef INTERFACE_H_INCLUDED
#define INTERFACE_H_INCLUDED

void gotoxy(int x, int y);
void cortexto(int nr);
void ecran();
void grelhaListaC();
void grelha_lista_res();
void desenha_vertical(int k);

int verifica_int(char *valor);
int verifica_opcao_menu(char* num, int total,int x);

#endif // INTERFACE_H_INCLUDED
