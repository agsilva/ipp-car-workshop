#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lista.h"

int compara_datas (int dia1, int mes1, int ano1, int dia2, int mes2, int ano2)
{
    if (ano1 > ano2)
        return 1;

    else if (ano1 < ano2)
            return -1;

    else
    {
        if (mes1 > mes2)
            return 1;

        else if (mes1 < mes2)
            return -1;

        else
        {
            if (dia1 > dia2)
                return 1;

            else if (dia1 < dia2)
                return -1;
        }
    }

    return 0;
}

int compara_tempo(int dia1, int mes1, int ano1, int dia2, int mes2, int ano2, int hora1, int min1, int hora2, int min2)
{
    int resultado;

    if((resultado=compara_datas(dia1,mes1,ano1,dia2,mes2,ano2))==0)
    {
        if (hora1 > hora2)
            return 1;
        else if (hora1 < hora2)
            return -1;
        else
        {
            if (min1 > min2)
                return 1;
            if (min1 < min2)
                return -1;
            return 0;
        }
    }

    return resultado;
}

void converte_data(int dia, int mes, int ano, char data[])
{
    if (dia < 10)
        if (mes < 10)
            sprintf(data,"0%d/0%d/%d",dia,mes,ano);
        else
            sprintf(data,"0%d/%d/%d",dia,mes,ano);
    else
        if (mes < 10)
            sprintf(data,"%d/0%d/%d",dia,mes,ano);
        else
            sprintf(data,"%d/%d/%d",dia,mes,ano);

}
void converte_hora(int hora, int min, char horas[])
{
    if (hora < 10)
        if (min < 10)
            sprintf(horas,"0%dh0%d",hora,min);
        else
            sprintf(horas,"0%dh%d",hora,min);
    else
        if (min < 10)
            sprintf(horas,"%dh0%d",hora,min);
        else
            sprintf(horas,"%dh%d",hora,min);
}

Date cria_no_data (int dia, int mes, int ano, int hora_i, int min_i, int hora_f, int min_f)
{
    Date data;

    if ((data = (Date)malloc(sizeof(Date_node))) != NULL)
    {
        data->dia=dia;
        data->mes=mes;
        data->ano=ano;
        data->hora_i=hora_i;
        data->min_i=min_i;
        data->hora_f=hora_f;
        data->min_f=min_f;
    }
    return data;
}

List_Res cria_lista_res (void)
{
    List_Res aux;

    aux = (List_Res) malloc (sizeof (List_Res_node));

    if (aux != NULL) {
        aux->info = NULL;
        aux->total=0;
        aux->next = NULL;
    }
    return aux;
}

List_Client cria_lista_client (void)
{
    List_Client aux;

    aux = (List_Client) malloc (sizeof (List_Client_node));

    if (aux != NULL) {
        aux->cliente = NULL;
        aux->total=0;
        aux->next = NULL;
    }
    return aux;
}

List_Client cria_no_client(char* nome, char* local, long int tele, long int nif)
{
    List_Client novo;

    if ((novo=(List_Client) malloc (sizeof(List_Client_node))) != NULL)
    {
        if ((novo->cliente = (Client) malloc (sizeof(Client_node))) != NULL)
        {
            strcpy(novo->cliente->nome,nome);
            strcpy(novo->cliente->local,local);
            novo->cliente->telefone=tele;
            novo->cliente->nif=nif;
            novo->cliente->res_l=cria_lista_res();
            novo->cliente->res_m=cria_lista_res();
            novo->cliente->pre_l=cria_lista_res();
            novo->cliente->pre_m=cria_lista_res();
        }
        novo->next=NULL;
    }

    return novo;
}

List_Res cria_no_res(Client cliente, Date data)
{
    List_Res nova;

    if ((nova=(List_Res) malloc (sizeof(List_Res_node))) != NULL)
    {
        if ((nova->info = (Reserva) malloc (sizeof(Reservation_node))) != NULL)
        {
            nova->info->data=data;
            nova->info->cliente = cliente;
        }

        nova->next = NULL;
    }

    return nova;
}

void insere_reserva_cliente (List_Res actual, int tipo)
{
    List_Res antR,inutil;
    List_Res antP,found;

    if (tipo==1){
        procura_lista (actual->info->cliente->res_l,actual->info->data->dia, actual->info->data->mes,actual->info->data->ano,actual->info->data->hora_i,actual->info->data->min_i,&antR,&inutil,1);
        procura_lista (actual->info->cliente->pre_l,actual->info->data->dia, actual->info->data->mes,actual->info->data->ano,actual->info->data->hora_i,actual->info->data->min_i,&antP,&found,0);
        actual->info->cliente->res_l->total++;
        actual->info->cliente->pre_l->total--;
    }
    else{
        procura_lista (actual->info->cliente->res_m,actual->info->data->dia, actual->info->data->mes,actual->info->data->ano,actual->info->data->hora_i,actual->info->data->min_i,&antR,&inutil,1);
        procura_lista (actual->info->cliente->pre_m,actual->info->data->dia, actual->info->data->mes,actual->info->data->ano,actual->info->data->hora_i,actual->info->data->min_i,&antP,&found,0);
        actual->info->cliente->res_m->total++;
        actual->info->cliente->pre_m->total--;
    }

    antP->next=found->next;
    found->next=antR->next;
    antR->next=found;
}

void insere_reserva (List_Res ant, List_Res actual)
{
    actual->next = ant->next;
    ant->next=actual;
}

List_Res adiciona_reserva (List_Res r, Date data, Client cliente, int tipo, int lav)
{
    List_Res no,novo;
    List_Res ant, inutil;

    no = cria_no_res(cliente,data);

    procura_lista (r, data->dia, data->mes,data->ano,data->hora_i,data->min_i, &ant, &inutil,-1);
    no->next = ant->next;
    ant->next = no;
    r->total++;

    if ((novo = (List_Res)malloc(sizeof(List_Res_node))) != NULL)
    {
        novo->info = no->info;
        novo->next = NULL;

        if (tipo==1)
        {
            if(lav==1){
                procura_lista (cliente->res_l,data->dia, data->mes,data->ano,data->hora_i,data->min_i,&ant,&inutil,1);
                cliente->res_l->total++;
            }
            else{
                procura_lista (cliente->res_m,data->dia, data->mes,data->ano,data->hora_i,data->min_i,&ant,&inutil,1);
                cliente->res_m->total++;
            }
        }
        else
        {
            if(lav==1){
                procura_lista (cliente->pre_l,data->dia, data->mes,data->ano,data->hora_i,data->min_i,&ant,&inutil,1);
                cliente->pre_l->total++;
            }
            else{
                procura_lista (cliente->pre_m,data->dia, data->mes,data->ano,data->hora_i,data->min_i,&ant,&inutil,1);
                cliente->pre_m->total++;
            }
        }
        novo->next = ant->next;
        ant->next=novo;
    }

    return novo;
}

/*percorre a lista ate a data (actual) ser mais antiga (1), mais recente (-1) ou igual(0)  � anterior*/
void procura_lista (List_Res lista, int dia, int mes,int ano, int hora, int min, List_Res *ant, List_Res *actual, int ordem)
{
    *ant = lista; *actual = lista->next;

    while ((*actual) != NULL && compara_tempo(dia,mes,ano,(*actual)->info->data->dia,(*actual)->info->data->mes,(*actual)->info->data->ano,hora,min,(*actual)->info->data->hora_i,(*actual)->info->data->min_i)!=ordem)
    {
        *ant = *actual;
        *actual = (*actual)->next;
    }
}

/*Destroi listas de reserva do cliente + estruturas partilhadas com outras listas (info + data)*/
void destroi_reservas_client(List_Res res)
{
    List_Res tempR, aux;

    aux = res->next;

    while(aux!=NULL) {
        tempR = aux;
        res->next = aux->next;
        aux=aux->next;
        free(tempR->info->data);
        free(tempR->info);
        free(tempR);
    }

    free(res);
}

/*Destroi listas de pre-reserva do cliente (so info, uma vez que a data estava associada a uma reserva)*/
void destroi_pRes_cliente(List_Res res)
{
    List_Res tempR, aux;

    aux = res->next;

    while(aux!=NULL) {
        tempR = aux;
        res->next = aux->next;
        aux=aux->next;
        free(tempR->info);
        free(tempR);
    }

    free(res);
}

/*Destroi listas de reserva e pre-reserva globais
//uma vez que todas as estruturas para onde estas listas apontavam,
ja foram libertadas pelas funcoes acima*/
void destroi_global_res(List_Res res)
{
    List_Res tempR, aux;

    aux = res->next;

    while(aux!=NULL) {
        tempR = aux;
        res->next = aux->next;
        aux=aux->next;
        free(tempR);
    }

    free(res);
}

void destroi_listas(List_Client c, List_Res r_m, List_Res r_l, List_Res p_m, List_Res p_l)
{
    List_Client tempC,aux;

    aux = c->next;

    while(aux!=NULL) {
        tempC = aux;
        c->next = aux->next;
        aux = aux->next;
        destroi_reservas_client(tempC->cliente->res_m);
        destroi_reservas_client(tempC->cliente->res_l);
        destroi_pRes_cliente(tempC->cliente->pre_m);
        destroi_pRes_cliente(tempC->cliente->pre_l);
        free (tempC->cliente);
        free(tempC);
    }

    free(c);

    destroi_global_res(r_m);
    destroi_global_res(r_l);
    destroi_global_res(p_m);
    destroi_global_res(p_l);
}
