#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "lista.h"

void carrega_clientes(List_Client c)
{
    List_Client novo, aux;
    char nome[60],local[60],line[60];
    long int tele, nif;
    int i=1;
    FILE *f_read=fopen("Clientes.txt","r");
/*  se ficheiro existir, vai ler cada bloco de 5 linhas para a variavel 'novo' e colocar na lista*/
    if ( f_read != NULL )
    {
        while (fgets(line, 60, f_read) != NULL)
        {
           /* substitui o \n que le de cada linha por \0 . ( \0 significa fim da string )*/
            if (line[strlen(line)-1]=='\n' && strlen(line)>1)
                line[strlen(line)-1]='\0';

            if (i==1)
                strcpy(nome,line);
            else
            {
                if (i==2)
                    strcpy(local,line);

                else
                {
                    if (i==3)
                        tele=atoi(line);
                    else
                    {
                        if (i==4)
                        {
                            nif=atoi(line);
                            novo=cria_no_client(nome,local,tele,nif);
                            if(c->next == NULL)
                                c->next = novo;
                            else{
                                aux = pesquisa_cliente(c,nome);
                                novo->next=aux->next;
                                aux->next = novo;
                            }
                            c->total++;
                        }
                        else
                            i=0;
                    }
                }

            }
            i++;
        }

        fclose (f_read);
   }
}

void guarda_clientes(List_Client c)
{
    FILE *f_write=fopen("Clientes.txt","w");
    c=c->next;

    while(c!=NULL)
    {
        fprintf(f_write,"%s\n",c->cliente->nome);
        fprintf(f_write,"%s\n",c->cliente->local);
        fprintf(f_write,"%ld\n",c->cliente->telefone);
        fprintf(f_write,"%ld\n",c->cliente->nif);
        fprintf(f_write,"\n");
        c=c->next;
    }
    fclose(f_write);
}

void carrega_reservas(List_Client c, List_Res r, List_Res res, char* texto)
{
    Client cliente;
    List_Res aux;
    Date data;
    char line [60];
    int dia, mes, ano, hora_i, hora_f, min_i, min_f;
    long int nif;
    int i=1, expirado=0, tipo=0,lav=0;

    struct tm *tempo = NULL;
    time_t Tval = 0;
    Tval = time(NULL);
    tempo = localtime(&Tval);

    FILE *f_read=fopen(texto,"r");
/*  se ficheiro existir, vai ler cada bloco de 5 linhas para a variavel 'novo' e colocar na lista*/
    if ( f_read != NULL )
    {
        if (strcmp("Reservas_lavagem.txt",texto)==0)
        {
            tipo = 1;
            lav = 1;
        }
        else
        {
            if (strcmp("Reservas_manutencao.txt",texto)==0)
            {
                tipo = 1;
                lav = 2;
            }
            else
            {
                if (strcmp("Pre_Reservas_lavagem.txt",texto)==0)
                {
                    tipo = 2;
                    lav = 1;
                }
                else
                {
                    if (strcmp("Pre_Reservas_lavagem.txt",texto)==0)
                    {
                        tipo = 2;
                        lav = 1;
                    }
                }
            }
        }

        while (fgets(line, 60, f_read) != NULL)
        {
            if (expirado==0)
            {
               /* substitui o \n que le de cada linha por \0 . ( \0 significa fim da string )*/
                if (line[strlen(line)-1]=='\n' && strlen(line)>1)
                    line[strlen(line)-1]='\0';

                if (i==1)
                {
                    dia=10*(line[0]-48)+(line[1]-48);
                    mes=10*(line[3]-48)+(line[4]-48);
                    ano=1000*(line[6]-48)+100*(line[7]-48)+10*(line[8]-48)+(line[9]-48);

                    if (compara_datas(dia,mes,ano,tempo->tm_mday,(1+tempo->tm_mon),(1900+tempo->tm_year))==-1)
                        expirado=1;
                }
                else
                {
                    if (i==2)
                    {
                        hora_i=10*(line[0]-48)+(line[1]-48);
                        min_i=10*(line[3]-48)+(line[4]-48);
                    }
                    else
                    {
                        if (i==3)
                        {
                            hora_f=10*(line[0]-48)+(line[1]-48);
                            min_f=10*(line[3]-48)+(line[4]-48);

                            if ((compara_datas(dia,mes,ano,tempo->tm_mday,(1+tempo->tm_mon),(1900+tempo->tm_year))==0) && (hora_f < tempo->tm_hour || (hora_f == tempo->tm_hour && min_f < tempo->tm_min)))
                                expirado=1;
                        }

                        else
                        {
                            if (i==4)
                            {
                                nif = atoi(line);
                                cliente = pesquisa_cliente_nif(c,nif);
                                if (cliente == NULL){   //caso haja algum problema na leitura de NIFs, a reserva fica como expirada
                                    expirado = 1;
                                    continue;
                                }
                                if(tipo==1)
                                {
                                    data = cria_no_data (dia, mes, ano, hora_i, min_i, hora_f, min_f);
                                    adiciona_reserva(r,data,cliente,tipo,lav);
                                }
                                else
                                {
                                    if (tipo==2)
                                    {
                                       for(aux=res->next;aux!=NULL;aux=aux->next)
                                            if(compara_tempo(dia,mes,ano,aux->info->data->dia,aux->info->data->mes,aux->info->data->ano,hora_i,min_i,aux->info->data->hora_i,aux->info->data->min_i)==0)
                                                break;
                                        adiciona_reserva(r,aux->info->data,cliente,tipo,lav);

                                    }

                                }
                            }
                            else
                                i=0;
                        }
                    }

                }
            }
            if(i==5)
            {
                i=0;
                expirado=0;
            }
            i++;
        }

        fclose (f_read);
   }
}

void guarda_reservas(List_Res r, char* texto)
{
    FILE *f_write=fopen(texto,"w");
    char data[12],horas[6];
    r=r->next;

    while(r!=NULL)
    {
        converte_data(r->info->data->dia,r->info->data->mes,r->info->data->ano,data);
        fprintf(f_write,"%s\n",data);

        converte_hora(r->info->data->hora_i,r->info->data->min_i,horas);
        fprintf(f_write,"%s\n",horas);

        converte_hora(r->info->data->hora_f,r->info->data->min_f,horas);
        fprintf(f_write,"%s\n",horas);

        fprintf(f_write,"%ld\n",r->info->cliente->nif);
        fprintf(f_write,"\n");
        r=r->next;
    }
    fclose(f_write);
}

void actualiza_clientes(List_Client c)
{
    FILE *f_write=fopen("Clientes.txt","a");

    fprintf(f_write,"%s\n",c->cliente->nome);
    fprintf(f_write,"%s\n",c->cliente->local);
    fprintf(f_write,"%ld\n",c->cliente->telefone);
    fprintf(f_write,"%ld\n",c->cliente->nif);
    fprintf(f_write,"\n");

    fclose(f_write);
}

void actualiza_reservas(List_Res r, char* texto)
{
    FILE *f_write=fopen(texto,"a");
    char data[12],horas[6];

    converte_data(r->info->data->dia,r->info->data->mes,r->info->data->ano,data);
    fprintf(f_write,"%s\n",data);

    converte_hora(r->info->data->hora_i,r->info->data->min_i,horas);
    fprintf(f_write,"%s\n",horas);

    converte_hora(r->info->data->hora_f,r->info->data->min_f,horas);
    fprintf(f_write,"%s\n",horas);

    fprintf(f_write,"%ld\n",r->info->cliente->nif);
    fprintf(f_write,"\n");

    fclose(f_write);
}

