/*PPP - Projecto 2014
Ana Rita P�scoa N� 2010129292
Rafael Vicente da Costa N� 2012138447
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <time.h>
#include "interface.h"
#include "lista.h"
#include "ficheiros.h"

/*cores*/
#define azul_escuro 1
#define verde_escuro 2
#define esverdeado 3
#define vermelho_escuro 4
#define dourado 6
#define original 7
#define cinzento 8
#define azul 9
#define verde 10
#define azul_claro 11
#define vermelho 12
#define amarelo 14

/* Funcoes de verificacao */
int verifica_opcao_reserva(char ini, char fin, int x, int y);
int verifica_dia();
int verifica_mes();
int verifica_ano(int dia, int mes, int *ano);
int verifica_data(int dia,int mes, int ano);
int verifica_hora();
int verifica_min(int dia, int mes, int ano, int hora, int *min, int tipo);
int verifica_horas(int dia, int mes, int ano, int hora, int min, int tipo);
Date verifica_reserva(List_Res r,int dia, int mes, int ano, int hora_i,int min_i, int hora_f, int min_f);

void cria_reserva(List_Client c, List_Res r, List_Res p, int tipo, char *texto);
void mostra_reserva(int opcao,List_Res r,char* tipo, char *titulo);

void lista_pReservas(List_Res r, int tipo, int lav, char* texto);

void cancela_reserva(List_Res r, List_Res p, int tipo);
void cancela_pre_reserva(List_Res r, int tipo);
int confirma_cancelamento_pre(List_Res r, List_Res ant, List_Res actual, int tipo);
int confirma_cancelamento_res(List_Res r, List_Res ant, List_Res actual, List_Res p, int tipo);
void mostra_cancela(List_Res actual, char *texto);

void lista_menu(List_Client head_c, List_Res head_res_man, List_Res head_res_lav, List_Res head_pre_man, List_Res head_pre_lav);
int menu();

/* Menus */
int menu1(char *texto);
int menu2(char *texto);

void data();

int main()
{
    /*cria os headers das listas*/
    List_Client head_c=cria_lista_client();

    List_Res head_res_lav=cria_lista_res();
    List_Res head_res_man=cria_lista_res();

    List_Res head_pre_lav=cria_lista_res();
    List_Res head_pre_man=cria_lista_res();


    /*carrega dos ficheiros de texto os dados*/
    carrega_clientes(head_c);
    carrega_reservas(head_c,head_res_lav,NULL,"Reservas_lavagem.txt");
    carrega_reservas(head_c,head_res_man,NULL,"Reservas_manutencao.txt");
    carrega_reservas(head_c,head_pre_lav,head_res_lav,"Pre_Reservas_lavagem.txt");
    carrega_reservas(head_c,head_pre_man,head_res_man,"Pre_Reservas_manutencao.txt");

    /*vai para o menu*/

    lista_menu(head_c,head_res_man,head_res_lav,head_pre_man,head_pre_lav);

    guarda_clientes(head_c);
    guarda_reservas(head_res_lav,"Reservas_lavagem.txt");
    guarda_reservas(head_res_man,"Reservas_manutencao.txt");
    guarda_reservas(head_pre_lav,"Pre_Reservas_lavagem.txt");
    guarda_reservas(head_pre_man,"Pre_Reservas_manutencao.txt");

    destroi_listas(head_c,head_res_man,head_res_lav,head_pre_man,head_pre_lav);
    return 0;
}

void lista_menu(List_Client head_c, List_Res head_res_man, List_Res head_res_lav, List_Res head_pre_man, List_Res head_pre_lav)
{
    int opcao,opcao1,opcao2;
    Client cliente;
    do{
		system("cls");
		fflush(stdin);
		opcao=menu();
		switch (opcao){
		    case 1: {
                fflush(stdin);
                cria_reserva(head_c,head_res_lav,head_pre_lav,1,"reserva para lavagem");
                break;}
            case 2: {
                fflush(stdin);
                cria_reserva(head_c,head_res_man,head_pre_man,2,"reserva para manutencao");
                break;}
            case 3: {
			    fflush(stdin);
			    if ((opcao1=menu1("reserva"))==1)
			    {
			        if (head_res_lav->total!=0)
			        {
                        if((opcao2=menu2("reserva"))==2)
                            lista_pReservas(head_res_lav,1,1," ");
                        else
                            if(opcao2==1)
                            {
                                cliente=lista_cliente(head_c,"Escolher o Cliente");
                                if (cliente!=NULL)
                                    lista_pReservas(cliente->res_l,1,1," deste cliente ");
                            }
			        }
			        else
                        lista_pReservas(head_res_lav,1,1," ");

			    }
                else
                    if(opcao1==2)
                    {
                        if (head_res_man->total!=0)
                        {
                            if((opcao2=menu2("reserva"))==2)
                                lista_pReservas(head_res_man,1,2," ");
                            else
                                if(opcao2==1){
                                    cliente=lista_cliente(head_c,"Escolher o Cliente");
                                    if (cliente!=NULL)
                                        lista_pReservas(cliente->res_m,1,2," deste cliente ");
                                }
                        }
                        else
                            lista_pReservas(head_res_man,1,2," ");

                    }

                break;}
            case 4: {
			    fflush(stdin);
			    if ((opcao1=menu1("pre-reserva"))==1)
			    {
                    if (head_pre_lav->total!=0)
			        {
                        if((opcao2=menu2("pre-reserva"))==2)
                            lista_pReservas(head_pre_lav,2,1," ");
                        else
                            if(opcao2==1)
                            {
                                cliente=lista_cliente(head_c,"Escolher o Cliente");
                                if (cliente!=NULL)
                                    lista_pReservas(cliente->pre_l,2,1," deste cliente ");
                            }
			        }
			        else
                        lista_pReservas(head_pre_lav,2,1," ");

			    }
                else
                    if(opcao1==2)
                    {
                        if (head_pre_man->total!=0)
                        {
                            if((opcao2=menu2("pre-reserva"))==2)
                                lista_pReservas(head_pre_man,2,2," ");
                            else
                                if(opcao2==1){
                                    cliente=lista_cliente(head_c,"Escolher o Cliente");
                                    if (cliente!=NULL)
                                        lista_pReservas(cliente->pre_m,2,2," deste cliente ");
                                }
                        }
                        else
                            lista_pReservas(head_pre_man,2,2," ");
                    }
				break;}
            case 5: {
			    fflush(stdin);
			    if ((opcao1=menu1("reserva"))==1)
                    cancela_reserva(head_res_lav,head_pre_lav,1);
                else
                    if(opcao1==2)
                        cancela_reserva(head_res_man,head_pre_man,2);
				break;}
			case 6: {
			    fflush(stdin);
			    if ((opcao1=menu1("pre-reserva"))==1)
                    cancela_pre_reserva(head_pre_lav,1);
                else
                    if(opcao1==2)
                        cancela_pre_reserva(head_pre_man,2);
                break;
            }
		}
    }while(opcao!=0);
}
/*Menu inicial com todas as opcoes disponiveis.*/
int menu()
{
	int opcao;
	ecran();
	cortexto(amarelo);
	gotoxy(30,3);printf("Esta%c%co de Servi%co",135,132,135);
	cortexto(original);
	gotoxy(13,8);printf("1 - > Reservar lavagem");
    gotoxy(13,9);printf("2 - > Reservar manuten%c%co",135,132);
	gotoxy(13,10);printf("3 - > Listar Reservas");
	gotoxy(13,11);printf("4 - > Listar Pre-Reservas");
    gotoxy(13,12);printf("5 - > Cancelar Reserva");
    gotoxy(13,13);printf("6 - > Cancelar Pre-Reserva");
	gotoxy(13,14);printf("0 - > Sair");
	gotoxy(36,19);data();
	cortexto(verde);
    gotoxy(7,22);printf("Escolha uma op%c%co: ",135,132);
    cortexto(original);

    opcao=verifica_opcao('0','6',26,22);

    return opcao;
}

int menu1(char *texto)
{
    int opcao;
	system("cls");
	ecran();
	cortexto(amarelo);
	gotoxy(30,3);printf("Tipo de %s",texto);
	cortexto(original);
	gotoxy(13,8);printf("1 - > Lavagem");
	gotoxy(13,9);printf("2 - > Manuten%c%co",135,132);
    gotoxy(13,11);printf("0 - > Voltar");
    cortexto(verde);
    gotoxy(7,22);printf("Escolha uma op%c%co: ",135,132);
	cortexto(original);
	fflush(stdin);

    opcao=verifica_opcao('0','2',26,22);

    return opcao;
}

int menu2(char *texto)
{
    int opcao;
	system("cls");
	ecran();
	cortexto(amarelo);
	gotoxy(30,3);printf("Listar %s",texto);
	cortexto(original);
	gotoxy(13,8);printf("1 - > Por cliente");
	gotoxy(13,9);printf("2 - > Por data");
    gotoxy(13,11);printf("0 - > Voltar");
    cortexto(verde);
    gotoxy(7,22);printf("Escolha uma op%c%co: ",135,132);
	cortexto(original);
	fflush(stdin);

    opcao=verifica_opcao('0','2',26,22);

    return opcao;
}

int confirma_reserva(int tipo, Client cliente, List_Res r,List_Res p, int dia, int mes, int ano, int hora_i, int min_i, int hora_f, int min_f)
{
    Date reservado;
    List_Res no;
    int opcao,j;
    char data[12],horas[6];
    system("cls");
    ecran();

    cortexto(amarelo);
    gotoxy(27,3);printf("Adicionar Reserva");
    cortexto(original);
    gotoxy(2,5);
    for (j=3;j<77;j++){
        printf("%c",196);
    }
    cortexto(azul);
    gotoxy(7,5);printf(" Cliente escolhido: ");
    cortexto(original);
    gotoxy(8,7);printf("Nome: %s",cliente->nome);
    gotoxy(8,8);printf("Localidade: %s",cliente->local);
    gotoxy(8,9);printf("Telefone: %ld",cliente->telefone);
    gotoxy(8,10);printf("NIF: %ld",cliente->nif);

    gotoxy(2,12);
    for (j=3;j<77;j++){
        printf("%c",196);
    }
    cortexto(azul);
    gotoxy(7,12);printf(" Reserva pretendida: ");
    cortexto(original);

    converte_data(dia,mes,ano,data);
    gotoxy(8,14);printf("Data: %s",data);

    converte_hora(hora_i,min_i,horas);
    gotoxy(8,16);printf("Hora Inicial: %s",horas);

    converte_hora(hora_f,min_f,horas);
    gotoxy(40,16);printf("Hora Final: %s",horas);

    cortexto(verde_escuro);
    gotoxy(7,22);printf("1- Sim    2- N%co    0-Alterar",132);

    reservado=verifica_reserva(r,dia,mes,ano,hora_i,min_i,hora_f,min_f);
    if (reservado==NULL){
        cortexto(verde);
        gotoxy(7,21);printf("Deseja confirmar e criar esta reserva?: ");
        opcao=verifica_opcao_reserva('0','2',47,21);
    }
    else
    {
        cortexto(vermelho);
        gotoxy(8,18);printf("Existe uma reserva sobreposta! ");
        cortexto(esverdeado);
        gotoxy(7,21);printf("Deseja criar automaticamente uma pre-reserva?: ");
        opcao=verifica_opcao_reserva('0','2',54,21);
    }

    fflush(stdin);

    if (opcao==1)
    {
        if (reservado==NULL)
        {
            reservado = cria_no_data(dia,mes,ano,hora_i,min_i,hora_f,min_f);

            if (tipo==1){
                no = adiciona_reserva(r,reservado,cliente,1,1);
                actualiza_reservas(no,"Reservas_lavagem.txt");
            }
            else{
                no = adiciona_reserva(r,reservado,cliente,1,2);
                actualiza_reservas(no,"Reservas_manutencao.txt");
            }

            cortexto(verde);
            gotoxy(7,21);printf("Reserva adicionada com sucesso.                    ");
        }
        else{
            if (tipo==1){
                no = adiciona_reserva(p,reservado,cliente,2,1);
                actualiza_reservas(no,"Pre_reservas_lavagem.txt");
            }
            else{
                no = adiciona_reserva(p,reservado,cliente,2,2);
                actualiza_reservas(no,"Pre_reservas_manutencao.txt");
            }

            cortexto(verde);
            gotoxy(7,21);printf("Pre-reserva adicionada com sucesso.                    ");
        }

        cortexto(verde_escuro);
        gotoxy(7,22);printf("Prima qualquer tecla para regressar ao menu...");
        cortexto(original);
        getch();
    }

    if (opcao==0)
        return 1;

    return 0;
}

void select_data(int *dia, int *mes, int *ano, int *hora_i, int *min_i,int tipo)
{
    int j, temp_ano, temp_min,i;

    system("cls");
    ecran();
    cortexto(amarelo);
    gotoxy(20,3);printf("Adicionar marca%c%co pretendida",135,132);
    cortexto(original);

    gotoxy(2,5);
    for (j=3;j<77;j++){
        printf("%c",196);
    }

    cortexto(esverdeado);
    gotoxy(8,5);printf(" Data: ");
    cortexto(original);
    gotoxy(8,7);printf("Dia: ");
    gotoxy(8,9);printf("Mes: ");
    gotoxy(8,11);printf("Ano: ");

    gotoxy(2,13);
    for (j=3;j<77;j++){
        printf("%c",196);
    }
    cortexto(esverdeado);
    gotoxy(8,13);printf(" Horas: ");
    cortexto(original);

    gotoxy(8,15);printf("Hora de Inicio: ");
    gotoxy(8,17);printf("Minutos: ");

    while (1){
        (*dia)=verifica_dia();
        (*mes)=verifica_mes();

        if (verifica_ano(*dia,*mes,&temp_ano)==1)
        {
            for (i=7;i<=9;i+=2){
                gotoxy(13,i);printf("                                 ");
            }
            cortexto(vermelho_escuro);
            gotoxy(13,11);printf("                                           Erro: Data invalida ");
            cortexto(original);
        }

        else
            break;
    }

    gotoxy(17,11);printf("                                                           ");
    (*ano)=temp_ano;

    while (1)
    {
        (*hora_i)=verifica_hora();

        if (verifica_min(*dia,*mes,*ano,*hora_i,&temp_min,tipo)==1)
        {
            gotoxy(24,15);printf("                                 ");
            cortexto(vermelho_escuro);
            gotoxy(17,17);printf("                                       Erro: Hora invalida ");
            cortexto(original);
        }

        else
            break;
    }

    (*min_i)=temp_min;

}

void cria_reserva(List_Client c, List_Res r, List_Res p, int tipo, char *texto)
{
    Client cliente;
    int opcao;
    int dia,mes,ano,hora_i,hora_f,min_i,min_f;

    do{
        if((opcao=menu_cliente(texto))==0)
            return;

        if (opcao==1)
            cliente=insere_cliente(c);

        else if (opcao==2){
            cliente=lista_cliente(c,"Escolher o Cliente");
        }

    }while(cliente==NULL);

    do{
        select_data(&dia,&mes,&ano,&hora_i,&min_i,tipo);

        if (tipo == 1){
            if (min_i==30){
                hora_f=hora_i+1;
                min_f=min_i-30;
            }
            else{
                hora_f=hora_i;
                min_f=min_i+30;
            }
        }
        else{
            hora_f = hora_i+1;
            min_f = min_i;
        }

    } while (confirma_reserva(tipo,cliente,r,p,dia,mes,ano,hora_i,min_i,hora_f,min_f)==1);
}

/*funcao para listar as reservas por data*/
void lista_pReservas(List_Res r, int tipo, int lav, char *texto)
{
    List_Res aux_r;
    char data[12],horas[6],opt[6];
    int total;

    int k,i=0,opcao;

	if(r->total==0)
	{
        system("cls");
		ecran();
		cortexto(amarelo);
		gotoxy(30,3);

        if(tipo==1){
            printf("Consultar Reservas");
            cortexto(vermelho);
            gotoxy(8,8); printf("N%co existem reservas%sna base de dados!",132,texto);
        }
        else{
            printf("Consultar Pre-Reservas");
            cortexto(vermelho);
            gotoxy(8,8); printf("N%co existem pre-reservas%sna base de dados!",132, texto);
        }

        cortexto(verde_escuro);
        gotoxy(6,22);printf("Prima qualquer tecla para regressar ao menu...");
        cortexto(original);
        getch();

	}

    else
    {
        /*total guarda o numero de elementos da lista*/
        total=r->total;
        aux_r=r->next;
        do {
            system("cls");
            ecran();
            grelha_lista_res();
            /*imprime titulo*/
            cortexto(amarelo);
            gotoxy(29,3);

            if(tipo==1)
                printf("Consultar Reservas");

            else
                printf("Consultar Pre-Reservas");

            cortexto(original);

             /*imprime a posicao das paginas, para o caso de 1 unica pagina*/
            k=0;
            cortexto(cinzento);
            gotoxy(60,5);printf(" Pagina %d de %d ",1+i/6,1+(total-1)/6);
            cortexto(original);

            if(total>i)
                /*imprime a lista ate um maximo de 6 de cada vez*/
                for (;aux_r!=NULL;aux_r=aux_r->next)
                {
                    i++;
                    k++;
                    gotoxy(3,6+2*k);printf("%d",i);

                    converte_data(aux_r->info->data->dia,aux_r->info->data->mes,aux_r->info->data->ano,data);
                    gotoxy(8,6+2*k);printf("%s",data);

                    converte_hora(aux_r->info->data->hora_i,aux_r->info->data->min_i,horas);
                    gotoxy(21,6+2*k);printf("%s",horas);

                    converte_hora(aux_r->info->data->hora_f,aux_r->info->data->min_f,horas);
                    gotoxy(30,6+2*k);printf("%s",horas);

                    gotoxy(38,6+2*k);printf("%.37s", aux_r->info->cliente->nome);

                    if (i%6==0)
                    {
                        aux_r=aux_r->next;
                        break;
                    }
                }
            /*le a opcao*/
            cortexto(verde_escuro);
            gotoxy(3,23);printf("Teclas:  (0) -> Sair    (-1)  -> Avan%car para a proxima pagina",135);
            cortexto(verde);
            gotoxy(3,22);printf("Escolha o n%c da reserva para mais detalhes: ",248);
            cortexto(original);

            /*se opcao for invalida*/
            sprintf(opt,"%d",i);
            opcao = verifica_opcao_menu(opt,total,47);
            cortexto(original);
        }while (opcao==-1);

        if (opcao!=0)
        {
            if (tipo==1)
            {
                if (lav==1)
                    mostra_reserva(opcao,r,"Reserva","lavagem");
                else
                    mostra_reserva(opcao,r,"Reserva","manutencao");
            }
            else
            {
                if (lav==1)
                    mostra_reserva(opcao,r,"Pre-reserva","lavagem");
                else
                    mostra_reserva(opcao,r,"Pre-Reserva","manutencao");
            }
        }
    }
}

void mostra_reserva(int opcao, List_Res r, char* tipo, char* titulo)
{
    int i=1,j;
    char data[12],horas[6];

    system("cls");
    ecran();

    r=r->next;

    while(r!=NULL)
    {
        if (i==opcao)
            break;
        i++;
        r=r->next;
    }

    cortexto(amarelo);
    gotoxy(35,3);printf("%s",tipo);
    cortexto(original);
    gotoxy(2,5);
    for (j=3;j<77;j++){
        printf("%c",196);
    }

    cortexto(azul);
    gotoxy(7,5);printf(" Cliente: ");
    cortexto(original);
    gotoxy(8,7);printf("Nome: %s",r->info->cliente->nome);
    gotoxy(8,8);printf("Localidade: %s",r->info->cliente->local);
    gotoxy(8,9);printf("Telefone: %ld",r->info->cliente->telefone);
    gotoxy(8,10);printf("NIF: %ld",r->info->cliente->nif);

    gotoxy(2,13);
    for (j=3;j<77;j++){
        printf("%c",196);
    }
    cortexto(azul);
    gotoxy(7,13);printf(" %s para %s: ",tipo,titulo);
    cortexto(original);

    converte_data(r->info->data->dia,r->info->data->mes,r->info->data->ano,data);
    gotoxy(8,15);printf("Data: %s",data);

    converte_hora(r->info->data->hora_i,r->info->data->min_i,horas);
    gotoxy(8,17);printf("Hora Inicial: %s",horas);

    converte_hora(r->info->data->hora_f,r->info->data->min_f,horas);
    gotoxy(40,17);printf("Hora Final: %s",horas);

    cortexto(verde_escuro);
    gotoxy(6,22);printf("Prima qualquer tecla para regressar ao menu...");
    cortexto(original);
    getch();
}

void cancela_reserva(List_Res r, List_Res p, int tipo)
{
    List_Res ant_r,aux_r;
    char data[12],horas[6], opt[6];
    int total,i, confirma;

    int k,opcao;

	if(r->total==0)
	{
        system("cls");
		ecran();
		cortexto(amarelo);
		gotoxy(30,3);

        printf("Cancelar Reserva");

		cortexto(vermelho);
		gotoxy(8,8); printf("N%co existem reservas na base de dados!",132);
        cortexto(verde_escuro);
        gotoxy(6,22);printf("Prima qualquer tecla para regressar ao menu...");
        cortexto(original);
        getch();

	}

    else
    {
        /*total guarda o numero de elementos da lista*/
        total=r->total;
        do{
            aux_r=r->next;
            i=0;
            do {
                system("cls");
                ecran();
                grelha_lista_res();
                /*imprime titulo*/
                cortexto(amarelo);
                gotoxy(29,3);
                printf("Cancelar Reserva");
                cortexto(original);

                 /*imprime a posicao das paginas, para o caso de 1 unica pagina*/
                k=0;
                cortexto(cinzento);
                gotoxy(60,5);printf(" Pagina %d de %d ",1+i/6,1+(total-1)/6);
                cortexto(original);

                if(total>i)
                    /*imprime a lista ate um maximo de 6 de cada vez*/
                    for (;aux_r!=NULL;aux_r=aux_r->next)
                    {
                        i++;
                        k++;
                        gotoxy(3,6+2*k);printf("%d",i);

                        converte_data(aux_r->info->data->dia,aux_r->info->data->mes,aux_r->info->data->ano,data);
                        gotoxy(8,6+2*k);printf("%s",data);

                        converte_hora(aux_r->info->data->hora_i,aux_r->info->data->min_i,horas);
                        gotoxy(21,6+2*k);printf("%s",horas);

                        converte_hora(aux_r->info->data->hora_f,aux_r->info->data->min_f,horas);
                        gotoxy(30,6+2*k);printf("%s",horas);

                        gotoxy(38,6+2*k);printf("%.37s", aux_r->info->cliente->nome);

                        if (i%6==0)
                        {
                            aux_r=aux_r->next;
                            break;
                        }
                    }
                /*le a opcao*/
                cortexto(verde_escuro);
                gotoxy(3,23);printf("Teclas:  (0) -> Sair    (-1)  -> Avan%car para a proxima pagina",135);
                cortexto(verde);
                gotoxy(3,22);printf("Escolha o n%c da reserva que pretende cancelar: ",248);
                cortexto(original);

                /*se opcao for invalida*/
                sprintf(opt,"%d",i);
                opcao = verifica_opcao_menu(opt,total,50);

                cortexto(original);
            }while (opcao==-1);

            if (opcao==0)
                break;

            i=1;
            ant_r=r;
            for(aux_r=r->next;aux_r!=NULL;aux_r=aux_r->next,ant_r=ant_r->next,i++)
                if(i==opcao)
                    break;

        }while((confirma=confirma_cancelamento_res(r,ant_r,aux_r,p,tipo))==0);
    }
}

int confirma_cancelamento_res(List_Res r, List_Res ant, List_Res actual, List_Res p, int tipo)
{
    int opcao;
    List_Res auxR_c, antR_c;
    List_Res aux_p,ant_p;
    char texto_r[30], texto_p[30];

    mostra_cancela(actual,"Reserva");

    fflush(stdin);

    opcao=verifica_opcao('0','2',47,21);

    if (opcao==1){
        ant->next=actual->next;

        if (tipo==1){
            antR_c=actual->info->cliente->res_l;
            sprintf(texto_r,"%s","Reservas_lavagem.txt");
            sprintf(texto_p,"%s","Pre_reservas_lavagem.txt");
        }
        else
        {
            antR_c=actual->info->cliente->res_m;
            sprintf(texto_r,"%s","Reservas_manutencao.txt");
            sprintf(texto_p,"%s","Pre_reservas_manutencao.txt");
        }

        antR_c->total--;

//        //vai � lista do cliente e retira-a
        for(auxR_c=antR_c->next;auxR_c!=NULL;auxR_c=auxR_c->next,antR_c=antR_c->next)
            if (auxR_c->info == actual->info){ //encontrei reserva do cliente a ser removida
                    antR_c->next = auxR_c->next;
                    free(auxR_c);                 //retiro reserva do cliente

                    if(p->next!=NULL){           //se pre-reservas nao estiver vazia, verificar se existe alguma com data == actual->info->reserva
                        for(ant_p=p,aux_p=p->next;aux_p!=NULL;aux_p=aux_p->next,ant_p=ant_p->next)
                            if(aux_p->info->data==actual->info->data){  // encontrou data igual numa pre-reserva
                                ant_p->next=aux_p->next;        //retiro a info das pre-reservas
                                insere_reserva(ant,aux_p);
                                insere_reserva_cliente(aux_p,tipo); //pre-reserva deste cliente passa a reserva
                                p->total--;
                                guarda_reservas(p,texto_p);
                                break;
                            }

                        if(aux_p==NULL){     //significa que nao encontrou pre-reservas para a mesma data
                            free(actual->info->data);
                            r->total--;
                        }
                    }

                    else
                    {
                        /*Retira-se por completo toda a info relacionada com a reserva (i.e data)*/
                       free(actual->info->data);
                       r->total--;

                    }

                break;
            }
        free(actual->info);
        free(actual);
        guarda_reservas(r,texto_r);

        cortexto(verde);
        gotoxy(7,21);printf("Reserva cancelada com sucesso.                    ");
        cortexto(verde_escuro);
        gotoxy(7,22);printf("Prima qualquer tecla para regressar ao menu...");
        cortexto(original);
        getch();
    }

    return opcao;
}

void cancela_pre_reserva(List_Res r, int tipo)
{
    List_Res ant_r,aux_r;
    char data[12],horas[6], opt[6];
    int total,i, confirma;

    int k,opcao;

	if(r->total==0)
	{
        system("cls");
		ecran();
		cortexto(amarelo);
		gotoxy(30,3);

        printf("Cancelar Pre-Reserva");

		cortexto(vermelho);
		gotoxy(8,8); printf("N%co existem pre-reservas na base de dados!",132);
        cortexto(verde_escuro);
        gotoxy(6,22);printf("Prima qualquer tecla para regressar ao menu...");
        cortexto(original);
        getch();

	}

    else
    {
        /*total guarda o numero de elementos da lista*/
        total=r->total;
        do{
            aux_r=r->next;
            i=0;
            do {
                system("cls");
                ecran();
                grelha_lista_res();
                /*imprime titulo*/
                cortexto(amarelo);
                gotoxy(29,3);
                printf("Cancelar Pre-Reserva");
                cortexto(original);

                 /*imprime a posicao das paginas, para o caso de 1 unica pagina*/
                k=0;
                cortexto(cinzento);
                gotoxy(60,5);printf(" Pagina %d de %d ",1+i/6,1+(total-1)/6);
                cortexto(original);

                if(total>i)
                    /*imprime a lista ate um maximo de 6 de cada vez*/
                    for (;aux_r!=NULL;aux_r=aux_r->next)
                    {
                        i++;
                        k++;
                        gotoxy(3,6+2*k);printf("%d",i);

                        converte_data(aux_r->info->data->dia,aux_r->info->data->mes,aux_r->info->data->ano,data);
                        gotoxy(8,6+2*k);printf("%s",data);

                        converte_hora(aux_r->info->data->hora_i,aux_r->info->data->min_i,horas);
                        gotoxy(21,6+2*k);printf("%s",horas);

                        converte_hora(aux_r->info->data->hora_f,aux_r->info->data->min_f,horas);
                        gotoxy(30,6+2*k);printf("%s",horas);

                        gotoxy(38,6+2*k);printf("%.37s", aux_r->info->cliente->nome);

                        if (i%6==0)
                        {
                            aux_r=aux_r->next;
                            break;
                        }
                    }
                /*le a opcao*/
                cortexto(verde_escuro);
                gotoxy(3,23);printf("Teclas:  (0) -> Sair    (-1)  -> Avan%car para a proxima pagina",135);
                cortexto(verde);
                gotoxy(3,22);printf("Escolha o N%c da pre-reserva que pretende cancelar: ",248);
                cortexto(original);

                /*se opcao for invalida*/
                sprintf(opt,"%d",i);
                opcao = verifica_opcao_menu(opt,total,54);

                cortexto(original);
            }while (opcao==-1);

            if (opcao==0)
                break;

            i=1;
            ant_r=r;
            for(aux_r=r->next;aux_r!=NULL;aux_r=aux_r->next,ant_r=ant_r->next,i++)
                if(i==opcao)
                    break;

        }while((confirma=confirma_cancelamento_pre(r,ant_r,aux_r,tipo))==0);
    }
}

int confirma_cancelamento_pre(List_Res r, List_Res ant, List_Res actual, int tipo)
{
    int opcao;
    List_Res aux_c, ant_c;
    char texto[30];

    mostra_cancela(actual,"Pre-reserva");

    fflush(stdin);
    opcao=verifica_opcao('0','2',54,21);

    if (opcao==1){
        ant->next=actual->next;

        if (tipo==1){
            ant_c=actual->info->cliente->pre_l;
            sprintf(texto,"%s","Pre_reservas_lavagem.txt");
        }
        else{
            ant_c=actual->info->cliente->pre_m;
            sprintf(texto,"%s","Pre_reservas_manutencao.txt");
        }
        ant_c->total--;

        for(aux_c=ant_c->next;aux_c!=NULL;aux_c=aux_c->next,ant_c=ant_c->next)
            if (aux_c->info == actual->info)
                break;

        ant_c->next=aux_c->next;
        free(aux_c->info);
        free(aux_c);
        free(actual);

        r->total--;
        guarda_reservas(r,texto);

        cortexto(verde);
        gotoxy(7,21);printf("Pre-reserva cancelada com sucesso.                    ");
        cortexto(verde_escuro);
        gotoxy(7,22);printf("Prima qualquer tecla para regressar ao menu...");
        cortexto(original);
        getch();
    }

    return opcao;
}

void mostra_cancela(List_Res actual, char *texto)
{
    int j;
    char data[12],horas[6];

    system("cls");
    ecran();

    cortexto(amarelo);
    gotoxy(27,3);printf("Cancelar %s",texto);
    cortexto(original);
    gotoxy(2,5);
    for (j=3;j<77;j++){
        printf("%c",196);
    }
    cortexto(azul);
    gotoxy(7,5);printf(" Cliente escolhido: ");
    cortexto(original);
    gotoxy(8,7);printf("Nome: %s",actual->info->cliente->nome);
    gotoxy(8,8);printf("Localidade: %s",actual->info->cliente->local);
    gotoxy(8,9);printf("Telefone: %ld",actual->info->cliente->telefone);
    gotoxy(8,10);printf("NIF: %ld",actual->info->cliente->nif);

    gotoxy(2,12);
    for (j=3;j<77;j++){
        printf("%c",196);
    }
    cortexto(azul);
    gotoxy(7,12);printf(" %s: ",texto);
    cortexto(original);

    converte_data(actual->info->data->dia,actual->info->data->mes,actual->info->data->ano,data);
    gotoxy(8,14);printf("Data: %s",data);

    converte_hora(actual->info->data->hora_i,actual->info->data->min_i,horas);
    gotoxy(8,16);printf("Hora Inicial: %s",horas);

    converte_hora(actual->info->data->hora_f,actual->info->data->min_f,horas);
    gotoxy(40,16);printf("Hora Final: %s",horas);

    cortexto(verde_escuro);
    gotoxy(7,22);printf("1- Sim    2- N%co    0-Alterar",132);


    cortexto(verde);
    gotoxy(7,21);printf("Deseja confirmar e cancelar esta %s?: ",texto);
    cortexto(original);
}

/***************** funcoes de verificacao *****************/
int verifica_opcao(char ini, char fin, int x, int y)
{
    char opcao[3];

    while (1)
    {
        cortexto(original);
        fflush(stdin);
        fgets(opcao,sizeof(opcao),stdin);

        if (opcao[1]!='\n' || opcao[0] < ini || opcao[0]> fin)
        {
            cortexto(vermelho_escuro);
            gotoxy(x,y);printf("          Erro: Op%c%co invalida ",135,132);
            cortexto(original);
            gotoxy(x,y);
		}
		else
			break;
    }

	return atoi(opcao);
}

int verifica_opcao_reserva(char ini, char fin, int x, int y)
{
    char opcao[3];
    int i;

    while (1)
    {
        gotoxy(x,y);
        cortexto(original);
        fflush(stdin);
        fgets(opcao,sizeof(opcao),stdin);

        if (opcao[1]!='\n' || opcao[0] < ini || opcao[0]> fin)
        {
            cortexto(vermelho_escuro);
            gotoxy(47,23);printf("        Erro: Op%c%co invalida ",135,132);
            cortexto(original);
            gotoxy(x,y);
            for (i=x;i<76;i++)
                printf(" ");
		}
		else
			break;
    }

	return atoi(opcao);
}


Date verifica_reserva(List_Res r,int dia, int mes, int ano, int hora_i,int min_i, int hora_f, int min_f)
{
    List_Res aux;

    for(aux=r->next;aux!=NULL;aux=aux->next)
        if(aux->info->data->ano==ano && aux->info->data->mes==mes && aux->info->data->dia==dia)
            if (aux->info->data->hora_i == hora_i && aux->info->data->min_i == min_i)
                return aux->info->data;
    return NULL;
}

/***************** funcoes auxiliares *****************/
int verifica_dia()
{
    char opcao[6];

    while (1)
    {
        cortexto(original);
        gotoxy(13,7);
        fflush(stdin);
        fgets(opcao,sizeof(opcao),stdin);

        if (strlen(opcao)==3 && opcao[0]>='0' && opcao[0]<='3')
            if (opcao[1]>='0' && opcao[1]<='9')
                if(!(opcao[0]=='3' && opcao[1]>='2' && opcao[1]<='9'))
                    break;


        if (strlen(opcao)==2 && opcao[0]>'0' && opcao[0]<='9')
            break;

        cortexto(vermelho_escuro);
        gotoxy(13,7);printf("                                          Erro: Op%c%co invalida ",135,132);
    }

    gotoxy(15,7);printf("                                                             ");

    return atoi(opcao);
}

int verifica_mes()
{
    char opcao[6];

    while (1)
    {
        cortexto(original);
        gotoxy(13,9);
        fflush(stdin);
        fgets(opcao,sizeof(opcao),stdin);

        if (strlen(opcao)==3 && (opcao[0]=='0' || opcao[0]=='1') && opcao[1]>='0' && opcao[1]<'3')
            break;

        if (strlen(opcao)==2 && opcao[0]>'0' && opcao[0]<='9')
            break;

        cortexto(vermelho_escuro);
        gotoxy(13,9);printf("                                          Erro: Op%c%co invalida ",135,132);
    }

    gotoxy(15,9);printf("                                                             ");

    return atoi(opcao);
}
int verifica_ano(int dia, int mes, int *ano)
{
    char opcao[10];
    int i;

    while (1)
    {
        cortexto(original);
        gotoxy(13,11);
        fflush(stdin);
        fgets(opcao,sizeof(opcao),stdin);
        if (strlen(opcao)==5){
            for(i=0; i<4; i++)
                if (opcao[i]<'0' || opcao[i]>'9')
                    break;

            if(i==4) {    //se o ano sao so numeros
                break;
            }
        }
        cortexto(vermelho_escuro);
        gotoxy(13,11);printf("                                          Erro: Op%c%co invalida ",135,132);
    }

    gotoxy(17,11);printf("                                                           ");

    (*ano)=atoi(opcao);

    return verifica_data(dia,mes,*ano);
}
int verifica_data(dia,mes,ano)
{
    int dia_mes[12]={31,29,31,30,31,30,31,31,30,31,30,31};
    struct tm *tempo = NULL;
    time_t Tval = 0;
    Tval = time(NULL);
    tempo = localtime(&Tval);

    if (dia>dia_mes[mes-1]) // se dia for maior que os numero de dias do mes
        return 1;

    if (ano%4!=0 && mes==2)     //se o ano nao for bissexto e a reserva para ser a 29/2
        if(dia==dia_mes[mes-1])
            return 1;

    if (compara_datas(dia,mes,ano,tempo->tm_mday,(1+tempo->tm_mon),(1900+tempo->tm_year))==-1)  //se a data for anterior 'a actual
        return 1;

    return 0;
}

int verifica_hora()
{
    char opcao[6];

    while (1)
    {
        cortexto(original);
        gotoxy(24,15);
        fflush(stdin);
        fgets(opcao,sizeof(opcao),stdin);

        if (strlen(opcao)==3){
            if ((opcao[0]=='0' && opcao[1]>='8' && opcao[1]<='9') || (opcao[0]=='1' && opcao[1]>='0' && opcao[1]<='7'))
                break;
            else
            {
                cortexto(vermelho_escuro);
                gotoxy(24,15);printf("                          Erro: Horario -> 8h - 18h ");
            }

        }

        else{
            if (strlen(opcao)==2){
                if (opcao[0]>='8' && opcao[0]<='9')
                    break;
                else
                {
                    cortexto(vermelho_escuro);
                    gotoxy(24,15);printf("                          Erro: Horario -> 8h - 18h ");
                }
            }

            else
            {
                cortexto(vermelho_escuro);
                gotoxy(24,15);printf("                               Erro: Op%c%co invalida ",135,132);
            }
        }
    }
    gotoxy(26,15);printf("                                                  ");

    return atoi(opcao);
}

int verifica_min(int dia, int mes, int ano, int hora, int *min, int tipo)
{
    char opcao[6];

    while (1)
    {
        cortexto(original);
        gotoxy(17,17);
        fflush(stdin);
        fgets(opcao,sizeof(opcao),stdin);

        if (strlen(opcao)==3)
        {
            if (tipo==1){
                if ((opcao[0]=='0' || opcao[0]=='3') && opcao[1]=='0')
                    break;

                else{
                    cortexto(vermelho_escuro);
                    gotoxy(17,17);printf("                            Erro: Lavagem de 30 em 30 mins ");
                }

            }
            if(tipo == 2){
                if (opcao[0]=='0' && opcao[1]=='0')
                    break;

                else{
                    cortexto(vermelho_escuro);
                    gotoxy(17,17);printf("                           Erro: Manuten%c%co de 1 em 1 hora ",135,132);
                }
            }
        }

        else
        {
            if (strlen(opcao)==2){
                if(opcao[0]=='0')
                    break;

                else{

                    if(tipo == 1)
                    {
                        cortexto(vermelho_escuro);
                        gotoxy(17,17);printf("                            Erro: Lavagem de 30 em 30 mins ");
                    }
                    else
                    {
                        cortexto(vermelho_escuro);
                        gotoxy(17,17);printf("                           Erro: Manuten%c%co de 1 em 1 hora ",135,132);
                    }
                }

            }

            else
            {
                cortexto(vermelho_escuro);
                gotoxy(17,17);printf("                                      Erro: Op%c%co invalida ",135,132);
            }
        }
    }

    (*min)=atoi(opcao);

    return  verifica_horas(dia,mes, ano, hora, *min, tipo);
}

int verifica_horas(int dia, int mes, int ano, int hora, int min, int tipo)
{
    struct tm *tempo = NULL;
    time_t Tval = 0;
    Tval = time(NULL);
    tempo = localtime(&Tval);

    if (compara_datas(dia,mes,ano,tempo->tm_mday,(1+tempo->tm_mon),(1900+tempo->tm_year))==0)  //se a data for igual 'a actual
        if (hora < tempo->tm_hour || (hora == tempo->tm_hour && min < tempo->tm_min))
            return 1;

    return 0;
}

/*funcao que le a data do sistema, e imprime-a por extenso*/
void data()
{
    char buffer[10];
    char terca[10],marco[10];
    sprintf(terca,"Ter%ca",135);
    sprintf(marco,"Mar%co",135);

    char *Dia[7] = {
                   "Domingo"  , "Segunda", terca, "Quarta",
                   "Quinta", "Sexta", "Sabado"
                 };
    char *Mes[12] = {
                     "Janeiro",   "Fevereiro", marco,    "Abril",
                     "Maio",       "Junho",     "Julho",     "Agosto",
                     "Setembro", "Outubro",  "Novembro", "Dezembro"
                    };
    struct tm *tempo = NULL;
    time_t Tval = 0;
    Tval = time(NULL);
    tempo = localtime(&Tval);
    cortexto(dourado);

    strftime (buffer,10,"%Hh%M",tempo);

    printf("%s, %d de %s de %d, %s", Dia[tempo->tm_wday], tempo->tm_mday, Mes[tempo->tm_mon], 1900 + tempo->tm_year,buffer);
    cortexto(original);
}


