#include <stdio.h>
#include <windows.h>

/*cores*/
#define azul_escuro 1
#define verde_escuro 2
#define esverdeado 3
#define vermelho_escuro 4
#define dourado 6
#define original 7
#define cinzento 8
#define azul 9
#define verde 10
#define azul_claro 11
#define vermelho 12
#define amarelo 14

/*funcao para posicionar o cursor na janela*/
void gotoxy(int x, int y)
{
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

/*funcao para alterar a cor do texto*/
void cortexto(int nr)
{
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), nr);
}

void desenha_vertical(int k)
{
    int i,j;

 	for(j=0; j<k; j++)
    {
        printf("\n %c",186);

        for(i=0;i<74;i++)
            printf(" ");

        printf("%c",186);
    }
}

/*desenha a moldura de todos os ecrans*/
void ecran()
{
	printf("\n %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n %c                                                                          %c",201,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,187,186,186);

    desenha_vertical(2);

	printf("\n %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c",204,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,185);

    desenha_vertical(14);

	printf("\n %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c",204,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,185);

    desenha_vertical(3);

	printf("\n %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c",200,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,188);
}


/*desenha a grelha para a lista de clientes*/
void grelhaListaC()
{
    int i,j;
    cortexto(azul);
    gotoxy(3,6);printf("N%c",248);
    gotoxy(27,6);printf("Nome");
    gotoxy(54,6);printf("Telefone");
    gotoxy(69,6);printf("NIF");
    cortexto(dourado);
    for (i=6;i<=18;i++)
        for (j=2;j<=75;j++)
            {
                gotoxy(j,i);
                if (i%2!=0)
                {
                    if (j==6 || j== 52 || j==64)
                        printf("%c",197);

                    else
                        printf("%c",196);
                }
                else
                    if (j==6 || j== 52 || j==64)
                        printf("%c",179);
            }
    cortexto(original);
}

/*desenha a grelha para a lista de reservas*/
void grelha_lista_res()
{
    int i,j;
    cortexto(azul);
    gotoxy(3,6);printf("N%c",248);
    gotoxy(11,6);printf("Data");
    gotoxy(21,6);printf("Inicio");
    gotoxy(31,6);printf("Fim");
    gotoxy(55,6);printf("Nome");
    cortexto(dourado);
    for (i=6;i<=18;i++)
        for (j=2;j<=75;j++)
            {
                gotoxy(j,i);
                if (i%2!=0)
                {
                    if (j==6 || j== 19 || j==28 || j==36)
                        printf("%c",197);

                    else
                        printf("%c",196);
                }
                else
                    if (j==6 || j== 19 || j==28 || j==36)
                        printf("%c",179);
            }
    cortexto(original);
}


int verifica_int(char *valor)
{
    int i;

    for(i=0; i<strlen(valor);i++)
        if(valor[i]<'0' || valor[i]>'9')
            if (valor[i]!='-')
                break;

    if (i!=strlen(valor))
        return 1;

    return 0;
}

int verifica_opcao_menu(char *num, int total, int x)
{
    char opcao[6];
    int i;

    while(1)
    {
        cortexto(original);
        gotoxy(x,22);
        fflush(stdin);
        fgets(opcao,sizeof(opcao),stdin);
        if (opcao[strlen(opcao)-1]=='\n')
            opcao[strlen(opcao)-1]='\0';

        if(strlen(opcao)<= strlen(num)+1)
        {
            if(verifica_int(opcao)==0)
            {
                if (atoi(opcao)>=-1 && atoi(opcao)<=atoi(num)){
                    if (atoi(opcao)==-1 && total==atoi(num)){
                        cortexto(vermelho_escuro);
                        gotoxy(45,21);printf("Erro: Nao existem mais paginas");
                        gotoxy(x,22);
                        for(i=x;i<76;i++)
                            printf(" ");
                        continue;
                    }
                    else
                        break;
                }
            }
        }
        cortexto(vermelho_escuro);
        gotoxy(45,21);printf("          Erro: Op%c%co invalida",135,132);
        gotoxy(x,22);
        for(i=x;i<76;i++)
            printf(" ");
    }

    return atoi(opcao);
}
