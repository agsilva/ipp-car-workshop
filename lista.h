#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED
#define MAX_NOME 60
#define MAX_LOCALIDADE 60

/*** Estrutura para Data ***/
typedef struct date *Date;
typedef struct date{
    int dia, mes, ano;
    int hora_i, min_i;
    int hora_f, min_f;
} Date_node;


/*** Estruturas e Lista de ponteiros para Pre-/Reserva ***/
typedef struct reservation *Reserva;
typedef struct lnode_res *List_Res;
typedef struct client *Client;
typedef struct lnode_client *List_Client;

/*** Estruturas e Lista de ponteiros para Cliente ***/
typedef struct client{
    char nome[MAX_NOME];
    char local[MAX_LOCALIDADE];
    long int telefone,nif;
    List_Res res_l;
    List_Res res_m;
    List_Res pre_l;
    List_Res pre_m;

} Client_node;

typedef struct lnode_client {
    Client cliente;
    int total;
    List_Client next;
} List_Client_node;
/***************************/

/*** Estruturas e Lista de ponteiros para Pre/Reserva ***/
typedef struct reservation{
    Client cliente;
    Date data;
} Reservation_node;

typedef struct lnode_res {
    Reserva info;
    int total;
    List_Res next;
} List_Res_node;
/***************************/

int compara_datas (int dia1, int mes1, int ano1, int dia2, int mes2, int ano2);
int compara_tempo(int dia1, int mes1, int ano1, int dia2, int mes2, int ano2, int hora1, int min1, int hora2, int min2);
void converte_data(int dia, int mes, int ano, char data[]);
void converte_hora(int hora, int min, char horas[]);

Date cria_no_data (int dia, int mes, int ano, int hora_i, int min_i, int hora_f, int min_f);
List_Res cria_lista_res (void);
List_Client cria_lista_client (void);
Client insere_cliente(List_Client c);
int verifica_opcao(char ini, char fin, int x, int y);
int menu_cliente(char *texto);

Client pesquisa_cliente_nif(List_Client c,long int nif);
Client lista_cliente(List_Client c, char* titulo);
List_Client cria_no_client(char* nome, char* local, long int tele, long int nif);
List_Res cria_no_res(Client cliente, Date data);
List_Res cria_no_pres(Client cliente, Date data);

void procura_cliente (List_Client c, char *nome, List_Client *actual);
List_Client pesquisa_cliente (List_Client c, char *nome);

void insere_reserva_cliente (List_Res actual, int tipo);
void insere_reserva (List_Res ant, List_Res actual);
List_Res adiciona_reserva(List_Res r, Date data, Client cliente, int tipo, int lav);
void procura_lista (List_Res lista, int dia, int mes,int ano, int hora, int min, List_Res *ant, List_Res *actual, int ant_rec);

void destroi_reservas_client(List_Res res);
void destroi_pre_global_res(List_Res res);
void destroi_listas (List_Client c, List_Res r_m, List_Res r_l, List_Res p_m, List_Res p_l);
long int verifica_numerosC(List_Client c,int x,int y,char *texto);

#endif // LISTA_H_INCLUDED
