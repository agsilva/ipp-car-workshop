#ifndef FICHEIRO_H_INCLUDED
#define FICHEIRO_H_INCLUDED

void carrega_clientes(List_Client c);
void guarda_clientes(List_Client c);

void carrega_reservas(List_Client c, List_Res r, List_Res res, char* texto);
void guarda_reservas(List_Res r, char* texto);
void actualiza_reservas(List_Res r, char* texto);
void actualiza_clientes(List_Client c);

#endif // FICHEIRO_H_INCLUDED
